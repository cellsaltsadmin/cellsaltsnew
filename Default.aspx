﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--=================================
 banner -->

    <%--    <section class="rev-slider">
        <div id="rev_slider_17_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-3" data-source="gallery" style="margin: 0px auto; background: transparent; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
            <!-- START REVOLUTION SLIDER 5.4.5.2 fullwidth mode -->
            <div id="rev_slider_17_1" class="rev_slider fullwidthabanner" style="display: none;" data-version="5.4.5.2">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-35" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="revolution/assets/slider-03/b894c-Bg.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption   tp-resizeme rs-parallaxlevel-3"
                            id="slide-35-layer-22"
                            data-x="-248"
                            data-y="505"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 5;">
                            <img src="revolution/assets/slider-03/73382-15.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                            id="slide-35-layer-23"
                            data-x="right" data-hoffset="-180"
                            data-y="-170"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 6;">
                            <img src="revolution/assets/slider-03/8f4ba-14.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption   tp-resizeme rs-parallaxlevel-2"
                            id="slide-35-layer-24"
                            data-x="right" data-hoffset="-200"
                            data-y="center" data-voffset="85"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 7;">
                            <img src="revolution/assets/slider-03/a5c1a-16.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>


                        <!-- LAYER NR. 10 -->
                        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                            id="slide-35-layer-11"
                            data-x="center" data-hoffset="-320"
                            data-y="235"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 14;">
                            <div class="rs-looped rs-slideloop" data-easing="" data-speed="3" data-xs="0" data-xe="10" data-ys="0" data-ye="10">
                                <img src="revolution/assets/slider-03/db7df-10.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                            </div>
                        </div>

                        <!-- LAYER NR. 12 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-35-layer-13"
                            data-x="center" data-hoffset="-60"
                            data-y="bottom" data-voffset="60"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 16;">
                            <img src="revolution/assets/slider-03/5536f-Shadow.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                        </div>



                        <!-- LAYER NR. 16 -->
                        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                            id="slide-35-layer-17"
                            data-x="right" data-hoffset="400"
                            data-y="bottom" data-voffset="30"
                            data-width="['none','none','none','none']"
                            data-height="['none','none','none','none']"
                            data-type="image"
                            data-responsive_offset="on"
                            data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 20;">
                            <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="2" data-xs="0" data-xe="10" data-ys="0" data-ye="10">
                                <img src="revolution/assets/slider-03/ea23f-8.png" alt="" data-ww="auto" data-hh="auto" data-no-retina>
                            </div>
                        </div>
                        <!-- LAYER NR. 17 -->
                        <div class="tp-caption   tp-resizeme"
                            id="slide-35-layer-18"
                            data-x="center" data-hoffset="410"
                            data-y="center" data-voffset="170"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"delay":370,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 21; white-space: nowrap; font-size: 24px; line-height: 44px; font-weight: 500; color: #ffffff; letter-spacing: 0px; font-family: Montserrat;">

                            <p>
                                Cell Salts are very important for the human body as they provide the basic nutrition to the body at the cellular level.
                                <br />
                                They combine with other vital components and helps maintain the millions of cell which form the body. When there is an imbalance of these important salts at the cellular level and imbalance is created.
                                <br />
                                This loss of balance gives rise to several types of diseases. These diseases can be cured by restoring the lost balance of the cell salts in the body.
                            </p>
                        </div>
                        <!-- LAYER NR. 18 -->
                        <div class="tp-caption rev-btn  tp-resizeme"
                            id="slide-35-layer-20"
                            data-x="center" data-hoffset="490"
                            data-y="bottom" data-voffset="199"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-type="button"
                            data-responsive_offset="on"
                            data-frames='[{"delay":840,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"600","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
                            data-textalign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[12,12,12,12]"
                            data-paddingright="[35,35,35,35]"
                            data-paddingbottom="[12,12,12,12]"
                            data-paddingleft="[35,35,35,35]"
                            style="z-index: 22; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px; font-family: Montserrat; text-transform: uppercase; background-color: rgb(0,0,0); border-color: rgba(0,0,0,1); border-radius: 30px 30px 30px 30px; outline: none; box-shadow: none; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; cursor: pointer;">
                            Start with us
                        </div>

                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
    </section>--%>

    <!--=================================
 banner -->


    <!--=================================
page-title-->

    <section class="page-title center light-overlay parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/b894c-Bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1 class="text-white text-center">Cell Salts For Health</h1>
                        <p class="text-white" style="color: #fff; font-size: 15px;">Cell Salts are very important for the human body as they provide the basic nutrition to the body at the cellular level. They combine with other vital components and helps maintain the millions of cell which form the body. When there is an imbalance of these important salts at the cellular level and imbalance is created. This loss of balance gives rise to several types of diseases. These diseases can be cured by restoring the lost balance of the cell salts in the body.</p>

                    </div>
                    <div class="col-lg-12 text-center">
                        <a class="button small" href="#">Know More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->

</asp:Content>

