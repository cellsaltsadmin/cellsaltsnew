﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cell_salt.aspx.cs" Inherits="cell_salt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/02.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>Cell Salts</h1>

                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="#">cell salts</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Cell Salts</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->
    <section class="page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Calcarea Fluorica</a>
                            </div>

                            <div class="entry-content">
                                <p>Calcarea Fluorica a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for stony hard glands, enlarged veins, cataracts, arteriosclerosis and for ligaments and tendons.Calc fluor is called the elasticity mineral. It is found in the cells of the bone, enamel of the teeth, and in the fibers of the skin. It is generally indicated for relaxed muscle tissue.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Calcarea Phosphorica</a>
                            </div>

                            <div class="entry-content">
                                <p>Calcarea Phosphorica is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for growing pains, Weak teeth, fractures, poor growth and rheumatism.Calc phos is called the cell mineral. This salt is needed for the formation of new cells, especially bone cells.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                    <%--                                    <a class="button arrow" data-toggle="modal" data-target=".bs-example-modal-lg" >Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>--%>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Calcarea Sulphuricum</a>
                            </div>

                            <div class="entry-content">
                                <p>Calcarea Sulphuricum is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy to clear-pus, skin and purify the blood.Calc sulph detoxifies the blood. This salt is generally used in suppurating conditions.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Ferrum Phosphoricum</a>
                            </div>

                            <div class="entry-content">
                                <p>Ferrum Phosphoricum is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for the symptoms of fever and the production of red blood cells.Ferrum phos is present in the composition of hemoglobin. It has the ability to draw oxygen from inhaled air and carry it to the blood.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Kali Muriaticum</a>
                            </div>

                            <div class="entry-content">
                                <p>Kali Muriaticum is a great little remedy and One of the twelve tissue salts of Schussler. It is a great remedy for the symptoms of swelling, inflammation and thick white mucous.Kali mur is well indicated for catarrh conditions and glandular swelling.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Kali Phosphorica</a>
                            </div>

                            <div class="entry-content">
                                <p>Kali Phosphorica is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for the symptoms of the brain, nerves, muscles and blood cells.Kali phos is a nerve cell mineral. This tissue salt is indicated in cases of weak, impatient, and emotional individuals.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Kali Sulphuricum</a>
                            </div>

                            <div class="entry-content">
                                <p>Kali Sulphuricum is the remedy for the late stage of an inflammation. In this phase inflammations are mostly not hot and red any more, instead, it comes to yellowish, sometimes suppurating seclusions.Kali sulph is similar to Ferrum phos. It helps carry oxygen to the blood. It is present in all cells that contain iron. This salt is well indicated for skin conditions.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Magnesia Phosphorica
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Magnesia Phosphorica is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for spasms, Cramps and pain.Mag phos is found in the nervous system. It has antispasmodic influences over muscle tissue.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Natrum Muriaticum
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Sodium chloride is another name for the cooking salt which we know well from our kitchen. In the body it plays an important role in the liquid metabolism and thus it is also applied as a cell salt.Nat mur has affinity with water distribution. It ensures a balance of moisture throughout the organism. It is indicated in conditions where too much water or dryness appears in the system.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Natrum Phosphoricum</a>
                            </div>

                            <div class="entry-content">
                                <p>Sodium phosphate is the cell salt of the metabolism. It can help if too much acid has led to health problems in the body. Sodium phosphate promotes the change of uric acid in urea what makes it removal easier. Nat phos is an essential salt for the digestive tract. It neutralizes acids and helps with the assimilation of fats and other nutrients. This salt is well indicated for symptoms of hyperacidity.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Natrum Sulphuricum
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Natrum Sulphuricum is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for the symptoms of the Water retention, urinary organs, liver, bile flow and digestion. Nat sulph helps excrete excessive water from the system.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Silicea
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Silicea is a great little remedy and one of the twelve tissue salts of Schussler. It is a great remedy for nails, hair, teeth and re-absorption of scar-tissue.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <button type="button" class="button small" data-toggle="modal" data-target=".bs-example-modal-lg">Read More</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>



    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="section-title mb-10">
                        <h6>Calcarea Fluorica</h6>
                        <h2>Calcarea Fluorica</h2>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <i class="text-center fa fa-leaf fa-4x mb-3 animated rotateIn teal-text"></i>
                    </div>
                    <div class="text-left">
                        <ul class="indications">
                            <li>Bone spurs, growths, bunions.</li>
                            <li>Joints that dislocate easily, swellings and enlarged joints, low back pains, knee problems.</li>
                            <li>Blur before the eyes after exerting the vision. Cataract.</li>
                            <li>Indurated infiltration of glands, cellular tissues and bony formations.</li>
                            <li>Nerve symptoms like sciatica, neuropathy, paralysis or neuritis.</li>
                            <li>The patient feels better in motion, warmth and after having warm drinks.</li>
                            <li>The patient is sensitive to cold, to drafts, to changes in the weather, and to damp weather. The symptoms are ameliorated by heat and by warm applications. The symptoms are worse during rest.</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

