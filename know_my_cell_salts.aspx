﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="know_my_cell_salts.aspx.cs" Inherits="know_my_cell_salts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/02.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1> Blogs</h1>

                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="#">Cell Salts</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Know My Cell Salts</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->
    <section class="page-section-ptb">
        <div class="container">
            <div class="row mt-10">
                <div class="col-lg-12 col-md-12 col-sm-12">

                    <div class="accordion gray plus-icon round mb-30">
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Mental States and Affections</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Mental States and Affections</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Eyes</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                         <div class="acd-group">
                            <a href="#" class="acd-heading">Ears</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                         <div class="acd-group">
                            <a href="#" class="acd-heading">Nose</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                         <div class="acd-group">
                            <a href="#" class="acd-heading">Face</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                         <div class="acd-group">
                            <a href="#" class="acd-heading">Mounth</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

