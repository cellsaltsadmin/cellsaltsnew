﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for dl_Usr_Registration
/// </summary>
public class dl_Usr_Registration : ReturnClass
{
    ReturnDataTable dt = new ReturnDataTable();
    ReturnBool rb = new ReturnBool();
    db_maria_connection db = new db_maria_connection();
    string query = null;

    public dl_Usr_Registration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public ReturnDataTable check_user_mobile(bl_Usr_Registration ur)
    {
        query = "SELECT * FROM user_registration WHERE applicant_mobile=@applicant_mobile";

        MySqlParameter[] pm = new MySqlParameter[]
            {
                new MySqlParameter("applicant_mobile",ur.Telephone_No)
            };

        dt = db.executeSelectQuery(query, pm);

        return dt;
    }

    public ReturnDataTable check_user_email(bl_Usr_Registration ur)
    {
        query = "SELECT * FROM user_registration WHERE applicant_email=@applicant_email";

        MySqlParameter[] pm = new MySqlParameter[]
            {
                new MySqlParameter("applicant_email",ur.Email)
            };

        dt = db.executeSelectQuery(query, pm);

        return dt;
    }

    public ReturnBool Insert_user_signup(bl_Usr_Registration ur)
    {
        query = @"insert into user_registration (applicant_first_name,applicant_last_name,applicant_email,applicant_mobile,
applicant_password,client_ip,applicant_active,applicant_mobile_verified,applicant_email_verified,date_time,role_id)
values(@applicant_first_name,@applicant_last_name,@applicant_email,@applicant_mobile,@applicant_password,@client_ip,
@applicant_active,@applicant_mobile_verified,@applicant_email_verified,@date_time,@role_id)";

        MySqlParameter[] pm = new MySqlParameter[]
            {
                new MySqlParameter("applicant_first_name",ur.Name),
                new MySqlParameter("applicant_last_name",ur.Last_name),
                new MySqlParameter("applicant_email",ur.Email),
                new MySqlParameter("applicant_mobile",ur.Cell_No),
                new MySqlParameter("applicant_password",ur.Password),
                new MySqlParameter("client_ip",ur.ClientIp),
                new MySqlParameter("applicant_active",ur.Applicant_active),
                new MySqlParameter("applicant_mobile_verified",ur.Applicant_mobile_verified),
                new MySqlParameter("applicant_email_verified",ur.Applicant_email_verified),
                new MySqlParameter("date_time",ur.Action_Date),
                new MySqlParameter("role_id",ur.Role_Id)
            };

        rb = db.executeInsertQuery(query, pm);

        return rb;
    }

    public ReturnDataTable user_login(bl_Usr_Registration ur)
    {
        query = "SELECT * FROM user_login WHERE applicant_email=@applicant or applicant_mobile=@applicant and applicant_verified=@applicant_verified";
        MySqlParameter[] pm = new MySqlParameter[]
            {
                new MySqlParameter("applicant",ur.Login_Id),
                new MySqlParameter("applicant_verified",ur.Verified)
            };
        dt = db.executeSelectQuery(query, pm);
        return dt;
    }
    public ReturnBool LoginTrail_new(LoginTrail lt)
    {
      query = @" INSERT INTO login_trail (user_id,client_ip,client_os,client_browser, useragent) 
                  VALUES (@user_id, @client_ip, @client_os, @client_browser, @useragent)";
        MySqlParameter[] pr = new MySqlParameter[]{
            new MySqlParameter("user_id",lt.User_Id),
            new MySqlParameter("client_ip",lt.Client_Ip),
            new MySqlParameter("client_os",lt.Client_Os),
            new MySqlParameter("client_browser",lt.Client_Browser),
            new MySqlParameter("useragent", lt.UserAgent)
        };

        rb = db.executeInsertQuery(query, pr);
        return rb;
    }

}