﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_login : System.Web.UI.Page
{
    bl_Usr_Registration bl = new bl_Usr_Registration();
    dl_Usr_Registration dl = new dl_Usr_Registration();
    ReturnClass.ReturnDataTable dt = new ReturnClass.ReturnDataTable();
    ReturnClass.ReturnBool rb = new ReturnClass.ReturnBool();
    Utilities util = new Utilities();
    string role_id = null;
    int rno;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Add("failed_attempt", "0");
            Random randomclass = new Random();
            Session["rno"] = randomclass.Next();
            rno = Convert.ToInt32(Session["rno"].ToString());
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["CheckRefresh"] = Session["CheckRefresh"];
    }
    protected void Login_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (Session["CheckRefresh"] != null)
                {
                    if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
                    {
                        Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
                        if (Session["rno"] == null)
                        {
                            Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Page has expired!!! Please open this page in a new window..");
                        }
                        else
                        {
                            bool cptch_expired = false;
                            try
                            {
                                Captcha1.ValidateCaptcha(txt_captcha.Text.Trim());
                            }
                            catch { cptch_expired = true; }
                            txt_captcha.Text = "";

                            if (!cptch_expired)
                            {
                                if (Captcha1.UserValidated)
                                {
                                    Captcha1.DataBind();
                                    if (!check_user())
                                    {
                                        Utilities.MessageBoxShow("Invaild User Id and Password..");
                                        fn_logintrail(false);
                                    }
                                    else
                                    {
                                        fn_logintrail(true);
                                        FormsAuthentication.Initialize();
                                        String strRole = role_id;
                                        FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, txt_login.Text, DateTime.Now, DateTime.Now.AddMinutes(30), false, strRole, FormsAuthentication.FormsCookiePath);
                                        Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(tkt)));
                                        Response.Redirect("User/DashBoard.aspx", false);
                                    }
                                }
                                else
                                {
                                    Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Invalid Captcha!!! Please enter same characters as you see in image.");
                                }
                            }
                            else
                            {
                                Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Captcha Expired!!! Please re open this page in new window.");
                            }
                        }
                    }
                    else
                        Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Page Refresh or Back button is now allowed");
                }
                else
                    Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Page expired!!! Please re open this page in new window.");
            }
            catch (NullReferenceException)
            {
                Utilities.MessageBox_UpdatePanel_Redirect(UpdatePanel2, "Your Session Has Expired Please Login Again", "../Logout.aspx");
            }
        }
    }


    public bool check_user()
    {
        bool fg = false;
        bl.Login_Id = txt_login.Text.Trim();
        bl.Password_Plain = Password.Text.Trim();
        bl.Password = bl.Password_Plain;
        bl.Seed = Session["rno"].ToString();
        bl.Verified = "Y";
        dt = dl.user_login(bl);
        if (dt.table.Rows.Count > 0)
        {
            Session["User_Id"] = dt.table.Rows[0]["applicant_mobile"].ToString();
            Session["Email_ID"] = dt.table.Rows[0]["applicant_email"].ToString();
            Session["Role_ID"] = dt.table.Rows[0]["role_id"].ToString();
            Session["name"] = dt.table.Rows[0]["applicant_first_name"].ToString() + " " + dt.table.Rows[0]["applicant_last_name"].ToString();
            fg = true;
        }
        else
        {
            Utilities.MessageBoxShow("Invalid User id or Password.");
        }  
            return fg;
        }

    public void fn_logintrail(bool flag)
    {
        LoginTrail lt1 = new LoginTrail();
        HttpBrowserCapabilities browser = Request.Browser;
        OperatingSystem os = Environment.OSVersion;
        lt1.User_Id = txt_login.Text.Trim();
        lt1.Client_Ip = util.GetClientIpAddress(this.Page);
        lt1.UserAgent = Request.UserAgent;
        if (lt1.UserAgent.Contains("Edge"))
            lt1.Client_Browser = "Microsoft Edge " + browser.Version;
        else
            lt1.Client_Browser = browser.Type;
        lt1.Client_Os = os.VersionString;
        rb = dl.LoginTrail_new(lt1);
    }
}