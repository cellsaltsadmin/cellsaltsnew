﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_signup : System.Web.UI.Page
{
    bl_Usr_Registration bl = new bl_Usr_Registration();
    dl_Usr_Registration dl = new dl_Usr_Registration();
    ReturnClass.ReturnDataTable dt = new ReturnClass.ReturnDataTable();
    ReturnClass.ReturnBool rb = new ReturnClass.ReturnBool();
    Utilities util = new Utilities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["CheckRefresh"] = Session["CheckRefresh"];
    }
    protected void Signup_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (Session["CheckRefresh"] != null)
                {
                    if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
                    {
                        Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
                        Captcha1.ValidateCaptcha(txt_captcha.Text.Trim());
                        txt_captcha.Text = "";
                        if (Captcha1.UserValidated)
                        {
                            Captcha1.DataBind();
                            bl.Name = txt_first_Name.Text.Trim();
                            bl.Last_name = txt_last_Name.Text.Trim();
                            bl.Cell_No = Mobile.Text.Trim();
                            bl.Email = Email.Text.Trim();
                            bl.Password_Plain = Password.Text.Trim();
                            bl.Password = bl.Password_Plain;
                            bl.Applicant_active = "N";
                            bl.Applicant_mobile_verified = "N";
                            bl.Applicant_email_verified = "N";
                            bl.Action_Date = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            bl.ClientIp = util.GetClientIpAddress(this.Page);
                            bl.Role_Id = "01";
                            rb = dl.Insert_user_signup(bl);
                            if (rb.status)
                            {
                                Utilities.MessageBox_UpdatePanel_Redirect(UpdatePanel2, "Record Saved Succesfully ", "otp_verification.aspx");

                            }
                            else
                            {
                                Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Records could not be saved: Please Try Again");

                            }
                        }
                        else
                        {
                            Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Invalid Captcha!!! Please enter same characters as you see in image.");
                        }
                    }
                    else
                        Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Page Refresh or Back button is now allowed");
                }
                else
                    Utilities.MessageBox_UpdatePanel(UpdatePanel2, "Page expired!!! Please re open this page in new window.");
            }
            catch (NullReferenceException)
            {
                Utilities.MessageBox_UpdatePanel_Redirect(UpdatePanel2, "Your Session Has Expired Please Login Again", "../Logout.aspx");
            }
        }

    }

    [WebMethod]
    public static string Check_user_mobile(string mobile)
    {
        string retval = "";
        ReturnClass.ReturnDataTable dt = new ReturnClass.ReturnDataTable();
        dl_Usr_Registration db = new dl_Usr_Registration();
        bl_Usr_Registration ur = new bl_Usr_Registration();
        ur.Telephone_No = mobile;
        dt = db.check_user_mobile(ur);
        if (dt.table.Rows.Count > 0)
        {
            retval = "false";
        }
        else
        {
            retval = "true";
        }
        return retval;
    }


    [WebMethod]
    public static string Check_user_email(string email)
    {
        string retval = "";
        ReturnClass.ReturnDataTable dt = new ReturnClass.ReturnDataTable();
        dl_Usr_Registration db = new dl_Usr_Registration();
        bl_Usr_Registration ur = new bl_Usr_Registration();
        ur.Email = email;
        dt = db.check_user_email(ur);
        if (dt.table.Rows.Count > 0)
        {
            retval = "false";
        }
        else
        {
            retval = "true";
        }
        return retval;
    }
}