﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="birthchart.aspx.cs" Inherits="birthchart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/02.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>Birth Chart and Cell Salts</h1>
                                          </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="#">Cell Salts</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Birth Chart and Cell Salts</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->
    <section class="page-section-ptb">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/aries.png" alt="Aries (March 21 - April 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Aries (March 21 - April 20)</h4>
                            <h6 class="cyan-text">Kali Phosphoricum (Potash-Potassium Phosphate)</h6>
                            <p class="text-justify">Nerve Tonic. In tissue and fluid of nerve, brain and muscle cells. For depression and sadness. Nerve issues like Shingles (very helpful for the pain right away every half hour. I have a personal story to share if it would be helpful.) Tension headaches, memory. With Mag. Phos. can restore good nerve functions. Helpful for timid, unhappy, fretful children. Aries/Mars rules: The head, red blood cells, the adrenals, male sexual organs and libido, vital energy, muscles, tendons and ligaments, primary brain, testosterone and progesterone.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/taurus.png" alt="Taurus (April 21 - May 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Taurus (April 21 - May 20)</h4>
                            <h6 class="cyan-text">Natrum Sulphuricum (Sodium Sulphate)</h6>
                            <p class="text-justify">Water Eliminator and Regulator. In blood vessel walls and cell coats.  For digestive issues, fatigue in the morning, liver/gallbladder upset, detoxes poisonous fluids, constipation. Colds that affect the throat (Taurus) and are rooted in Digestive System. Ailments from humid, damp conditions. Taurus/Venus rules: Kidneys, white blood cells, nose, receptor cells, spleen, tongue, inner brain, estrogen, cortisol, neck, throat, capillaries, veins, arteries.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/gemini.png" alt="Gemini (May 21 - June 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Gemini (May 21 - June 20)</h4>
                            <h6 class="cyan-text">Kali Muriaticum (Potassium Chloride)</h6>
                            <p class="text-justify">Glandular Tonic. In every body tissue except bone. For mucous congestion, glandular swelling, coughs especially chronic inflammation. Helps form fibrin in the blood, which removes  mucous discharges. Ears, throat, lungs, tonsils. Headaches if constipated, white tongue coating or liver is sluggish. With Kali. Mur. take at first sign of illness. Gemini/Mercury rules: Anatomy of ears, central nervous system, motor nerves, shoulders, arms and hands, sciatic nerves, lungs, bronchioles, tubes of the body, dexterity, short term memory.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/cancer.png" alt="Cancer (June 21 - July 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Cancer (June 21 - July 20)</h4>
                            <h6 class="cyan-text">Calcarea Fluorica (Calcium Fluoride)</h6>
                            <p class="text-justify">Elasticity. In surface of bones, blood vessels and muscle tissue. For varicose veins, cracked skin, hemorrhoids, muscle and ligament injuries. The containers of the body are nourished like the air sacks and stomach. Helps with emphysema, loose teeth, diseases of the bone surface, muscle elasticity. Symptoms are worse in humid conditions. Cancer/Moon rules: Containers of the body, fluids, the right eye in women, the left eye in men, optical nerve, retina, pupil, tear ducts, bladder, female sexual organs, loose connective tissues, mucous membranes.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/leo.png" alt="Leo (July 21 - August 22)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Leo (July 21 - August 22)</h4>
                            <h6 class="cyan-text">Magnesium Phosphate</h6>
                            <p class="text-justify">Nerve and Muscle Relaxant. In blood, bone and teeth. It is an anti-spasmodic. Helps menstrual and other cramps, hiccups, neuralgia, sciatica, headaches with stabbing pains, flatulence (can be a deficiency of magnesium). Magnesium is an important preventive mineral against heart attacks. Convulsions. Works well dissolved taken with a bit of hot water. Leo/Sun rules: Cardiovascular system, circulation, heart, left eye in women, right eye in men, life force, vitality.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/virgo.png" alt="Virgo (August 23 - September 22)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Virgo (August 23 - September 22)</h4>
                            <h6 class="cyan-text">Kali Sulphuricum (Potash-Potassium Sulphate)</h6>
                            <p class="text-justify">Skin Salt. In cells lining the skin. Works with Ferrum Phosphate to carry oxygen from the blood to the tissue cells. Helps distribute oils in the body thus good for digestive process. Good when there is a sticky yellow discharge from skin or mucous membrane. For hair, dry scaly skin, dandruff, chicken pox, also intestinal disorders and inflammatory conditions to promote sweating. Virgo/Mercury rules: Small intestines, digestion, the anatomy of ears, central nervous system, motor nerves, shoulders, arms and hands, sciatic nerves, lungs, bronchioles, tubes of the body, dexterity, short term memory.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/libra.png" alt="Libra (September 23 - October 22)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Libra (September 23 - October 22)</h4>
                            <h6 class="cyan-text">Natrum Phosphoricum (Sodium Phosphate)</h6>
                            <p class="text-justify">Natrum Phosphoricum (Sodium Phosphate) is the Libra cell salt. Libra rules the kidneys and lower back, and the acid/alkaline balance of the body. Nat phos. prevents excess acidity or alkalinity, especially in the bloodstream. It assists the kidneys in their function and is used to treat gout, kidney stones, ulcers and stomach acidity.  Emotionally Nat phos. is helpful in restoring emotional equilibrium, especially after mental exertion, or exposure to extremely stressful or tense environments.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/scorpio.png" alt="Scorpio (October 23 - November 21)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Scorpio (October 23 - November 21)</h4>
                            <h6 class="cyan-text">Calcarea Sulphuricum</h6>
                            <p class="text-justify">Calcarea Sulphuricum is the Scorpio cell salt. Calc Sulph is an important constituent of all connective tissue and essential to the healing process as a purifying agent. Scorpio rules the colon, sexual organs, large intestine, eliminative channels and outlets and the prostate gland. Calc sulph provides a protective coating to these organs. When it is lacking we get boils, skin eruptions, fistula or chronic constipation or diarrhea. Another manifestation is barrenness or impotence, as the body is incapable of providing a competent protective coating for egg and sperm.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/sagittarius.png" alt="Sagittarius (November 23 - December 22)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Sagittarius (November 23 - December 22)</h4>
                            <h6 class="cyan-text">Silicea</h6>
                            <p class="text-justify">Silicea is the cell salt for those born under the sign of Sagittarius. It is of use whenever there is pus to be discharged, such as in boils, abscesses or splinters. Taking silica after surgery helps minimize scar formation, and can assist the body to expel foreign objects such as splinters. Sagittarius rules the liver, hips, thighs, sciatic nerve and autonomic nervous system. Sagittarians under stress use up their Silica, leaving themselves prone to chronic problems involving liver function or hip degeneration later on in life. Silica is an important constituent of the cells of the connective tissue and the epidermis, of the bones, teeth, even the lens of the eye. Inadequate silica can make the teeth susceptible to decay, the hair dull and the nails brittle.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/capricorn.png" alt="Capricorn (December 23 - January 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Capricorn (December 23 - January 20)</h4>
                            <h6 class="cyan-text">Calcarea Phosphorica</h6>
                            <p class="text-justify">Calcarea Phosphorica is the cell salt of Capricorn. It is an important constituent of the bones. Capricorn rules the skeletal system, especially the teeth, the knees and the joints, the gall bladder and the skin. The body requires larger amounts of Calc phos. than any other, especially during childhood and growth spurts, or when recovering from broken bones. Skeletal problems such as rickets, curvature of the spine and tooth decay respond well to this cell salt. Elderly people are particularly responsive to Calc phos. because it not only adds calcium to the system but improves gastric digestive function so all vitamins and minerals are better assimilated. Pregnant women find this form of calcium gentle and well received by the body, as do children, who can develop overly strong bones too early if given supplemental calcium in other forms.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/aquarius.png" alt="Aquarius (January 21 - February 19)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Aquarius (January 21 - February 19)</h4>
                            <h6 class="cyan-text">Natrum Muriaticum (Sodium Chloride)</h6>
                            <p class="text-justify">Natrum Muriaticum (Sodium Chloride) is the cell salt of Aquarius. Nat mur. has the effect of attracting or drawing away water from affected parts of the body, to redistribute it wherever it is needed. Symptoms of insufficient Nat mur. are associated with watery colds, dryness or excessive salivation of the mouth, constipation, malaria, even herpes or any watery blisters on the skin or mucus membranes. Aquarius rules the blood and circulation, the ankles, the spinal cord, and the electrical force of the nerves. Nat mur. supports the smooth and even flow of electricity through and across the nerves. Lack of Nat mur. can cause sleeplessness and prevent proper nerve synapse firing- with consequent learning and speaking difficulties, and intermittent nerve pains.</p>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <a class="media-left waves-light waves-effect waves-light">
                            <img class="rounded-circle" src="http://cellsalts4health.com/images/zodiac/pisces.png" alt="Pisces (February 20 - March 20)">
                        </a>
                        <div class="media-body text-left pl-2">
                            <h4 class="media-heading teal-text">Pisces (February 20 - March 20)</h4>
                            <h6 class="cyan-text">Ferrum Phosphoricum</h6>
                            <p class="text-justify">Ferrum Phosphoricum is the cell salt for Pisces. It is the only common metal salt among the twelve cell salts, and is critically important in its function of making all of the other cell salts  more effective.  It is required for healthy red blood cells; lack of it can cause anemia. Pisces rules the lymphatic system and the feet. Ferrum phos. distributes oxygen to all body organs and tissues, and is indicated in all cases of inflammation and fever. It enables the blood and the lymph to carry waste and poisons away from affected tissue. It is especially indicated for people who are nervous and sensitive, who come down with every transient cold or flu.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

