﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="blog.aspx.cs" Inherits="blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/02.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>Blogs</h1>

                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="#">Cell Salts</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Blogs</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->


    <!--=================================
 Blog-->

    <section class="blog white-bg page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-responsive" src="images/blog/01.jpg" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost With Image</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <div class="owl-carousel bottom-left-dots" data-nav-dots="ture" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                                <div class="item">
                                    <img class="img-responsive" src="images/blog/02.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="images/blog/03.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="img-responsive" src="images/blog/04.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost With slider</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Quae laboriosam sunt hic perspiciatis, Consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="entry-soundcloud">
                            <iframe style="height: 300px; width: 100%;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/118951274&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost Audio Soundcloud</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Labore et dolore magna aliqua consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="blog-entry-html-video clearfix">
                            <video style="width: 100%; height: 100%;" id="player1" poster="video/video.jpg" controls preload="none">
                                <source type="video/mp4" src="video/video.mp4" />
                                <source type="video/webm" src="video/video.webm" />
                                <source type="video/ogg" src="video/video.ogv" />
                            </video>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost with Self Hosted Video (HTML5 Video)</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Veniam omnis nihil! A ea maiores corporis consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam . this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="blog-entry-audio audio-video">
                            <audio id="player2" src="video/audio.mp3"></audio>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost with Audio</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Tempor incididunt ut labore et dolore magna aliqua consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="clearfix">
                            <ul class="grid-post">
                                <li>
                                    <div class="portfolio-item">
                                        <img class="img-responsive" src="images/blog/05.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="portfolio-item">
                                        <img class="img-responsive" src="images/blog/06.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="portfolio-item">
                                        <img class="img-responsive" src="images/blog/07.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div class="portfolio-item">
                                        <img class="img-responsive" src="images/blog/08.jpg" alt="">
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost With Image Grid Gallery Format </a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, </p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="blog-entry-you-tube">
                            <div class="js-video [youtube, widescreen]">
                                <iframe src="https://www.youtube.com/embed/nrJtHemSPW4?rel=0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost with Youtube Video </a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! </p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="blog-entry-vimeo">
                            <div class="js-video [vimeo, widescreen]">
                                <iframe src="https://player.vimeo.com/video/176916362"></iframe>
                            </div>
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost with vimeo Video </a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Quae laboriosam sunt hic perspiciatis, asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. </p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ================================================ -->


                    <div class="blog-entry blockquote mb-50">
                        <div class="entry-blockquote clearfix">
                            <blockquote class="mt-60 ">
                                The trouble with programmers is that you can never tell what a programmer is doing until it's too late. The future belongs to a different kind of person with a different kind of mind: artists, inventors, storytellers-creative and holistic ‘right-brain’ thinkers whose abilities mark the fault line between who gets ahead and who doesn’t.
                  <cite class="text-white">– Daniel Pink</cite>
                            </blockquote>
                        </div>
                        <div class="blog-detail mt-30">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost With blockquote </a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-share mt-20 clearfix">
                                <div class="entry-button">
                                    <a class="button arrow white" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong class="text-white">Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- ================================================ -->
                    <div class="blog-entry mb-50">
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="#">Blogpost Without Image</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><i class="fa fa-folder-open-o"></i><a href="#">Design,</a> <a href="#">HTML5 </a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i>5</a></li>
                                    <li><a href="#"><i class="fa fa-calendar-o"></i>12 Aug 2018</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                <p>Asperiores mollitia excepturi voluptatibus sequi nostrum ipsam veniam omnis nihil! A ea maiores corporis. this exercise dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur, assumenda provident this exercise dolor sit amet, consectetur adipisicing elit. Quae laboriosam sunt hic perspiciatis, </p>
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="social list-style-none pull-right">
                                    <strong>Share : </strong>
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-dribbble"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- ================================================ -->
                    <div class="entry-pagination">
                        <div class="pagination-nav text-center">
                            <ul class="pagination">
                                <li><a href="#">«</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- ================================================ -->
                </div>
            </div>
        </div>
    </section>

    <!--=================================
 Blog-->
</asp:Content>

