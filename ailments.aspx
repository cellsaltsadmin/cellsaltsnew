﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ailments.aspx.cs" Inherits="ailments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--=================================
page-title-->

    <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/02.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title-name">
                        <h1>Ailments</h1>

                    </div>
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="#">Cell Salts</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Ailments</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
page-title -->

    <section class="page-section-ptb theme-bg">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">

                    <div class="contact-form dark-form clearfix ">
                        <div class="col-sm-10">
                            <input type="text" placeholder="e.g. Abscess" class="form-control" name="name">
                        </div>

                        <div class="col-sm-2">
                            <input type="hidden" name="action" value="sendEmail">
                            <button name="submit" type="submit" value="Send" class="button white"><span>Search </span><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="page-section-ptb">
        <div class="container">
            <div class="col-sm-12">
                <h3 class="text-center">Search By A-Z</h3>
                <nav>

                    <ul class="pagination">

                        <li><a href="#">A</a></li>
                        <li class="active"><a href="#">B <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">C</a></li>
                        <li><a href="#">D</a></li>
                        <li><a href="#">E</a></li>
                        <li><a href="#">F</a></li>
                        <li><a href="#">G</a></li>
                        <li><a href="#">H</a></li>
                        <li><a href="#">I</a></li>
                        <li><a href="#">J</a></li>
                        <li><a href="#">K</a></li>
                        <li><a href="#">L</a></li>
                        <li><a href="#">M</a></li>
                        <li><a href="#">N</a></li>
                        <li><a href="#">O</a></li>
                        <li><a href="#">P</a></li>
                        <li><a href="#">Q</a></li>
                        <li><a href="#">R</a></li>
                        <li><a href="#">S</a></li>
                        <li><a href="#">T</a></li>
                        <li><a href="#">U</a></li>
                        <li><a href="#">V</a></li>
                        <li><a href="#">W</a></li>
                        <li><a href="#">X</a></li>
                        <li><a href="#">Y</a></li>
                        <li><a href="#">Z</a></li>

                    </ul>
                </nav>
            </div>

        </div>
    </section>
    <section class="page-section-ptb">
        <div class="container">
            <div class="row mt-10">
                <div class="col-lg-12 col-md-12 col-sm-12">

                    <div class="accordion gray plus-icon round mb-30">
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Mental States and Affections</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Mental States and Affections</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading">Eyes</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                        <div class="acd-group">
                            <a href="#" class="acd-heading">Ears</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                        <div class="acd-group">
                            <a href="#" class="acd-heading">Nose</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                        <div class="acd-group">
                            <a href="#" class="acd-heading">Face</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>

                        <div class="acd-group">
                            <a href="#" class="acd-heading">Mounth</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

